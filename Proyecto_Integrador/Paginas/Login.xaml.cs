﻿using Proyecto_Integrador.Base;
using Proyecto_Integrador.Models;
using Proyecto_Integrador.Repositories;
using Plugin.Geolocator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Xaml;

namespace Proyecto_Integrador.Paginas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage
    {
       
        /*  <Label Text = "Coordenadas" />
             < Label Text="Latitud Y" />
             <Label x:Name="lat" />
             <Label Text = "Longitud X" />
             < Label x:Name="lon"/>
             <Label Text = "Altura h" />
             < Label x:Name="altimetry"/>*/
        //public List<MiMenu> MiMenu { get; set; }
        int count = 0; //Contador para en numero de clicks
        void Handle_Clicked(object sender, System.EventArgs e)//metodo para la la acción del boton
        {
            count++;
            ((Button)sender).Text = $"Has marcado {count} ubicacion(es).";
        }
        public Login()
        {
            //initializePlugin();
            InitializeComponent();
            GeneratePins();
            btnAgregar.Clicked += BtnAgregar_Clicked;
        }
        private void BtnAgregar_Clicked(object sender, EventArgs e)
        {
            ((NavigationPage)this.Parent).PushAsync(new Camara_Datos());
        }
        /* private async void initializePlugin()
         {
             if (!CrossGeolocator.IsSupported)
             {
                 await DisplayAlert("Error", "ha ocurrido un error al cargar el plugin", "OK");
                 return;
             }
             CrossGeolocator.Current.PositionChanged += Current_PositionChanged;// Se llama al metodo
             await CrossGeolocator.Current.StartListeningAsync(new TimeSpan(0, 0, 2), 0.5); //cada cuando se hará la geolocalización



         }*/
        /*private void Current_PositionChanged(object sender, Plugin.Geolocator.Abstractions.PositionEventArgs e)
        {
            if (!CrossGeolocator.Current.IsListening)
            {
                return;
            }
            var position = CrossGeolocator.Current.GetPositionAsync();

            lat.Text = position.Result.Latitude.ToString();
            lon.Text = position.Result.Longitude.ToString();
            altimetry.Text = position.Result.Altitude.ToString();
        }*/
        
        private void GeneratePins()
        {
            
            

            
            //latitud2 = Convert.ToDouble(obj.BuscarL1(2));
            //latitud3 = Convert.ToDouble(obj.BuscarL1(3));

            
            //altura2 = Convert.ToDouble(obj.BuscarL2(3));
            //altura3 = Convert.ToDouble(obj.BuscarL2(3));*/

        var pins = new List<Pin>
            {                
                               
                
                new Pin { Type = PinType.Place, Label = "Terreno 1", Address = "Eafit", Position = new Position(6.201468, -75.579083) },
                 new Pin { Type = PinType.Place, Label = "Terreno 2", Address = "Centro", Position = new Position(6.199217, -75.579222) },
                  new Pin { Type = PinType.Place, Label = "Terreno 3", Address = "Sur", Position = new Position(6.198822, -75.580896) },
                   new Pin { Type = PinType.Place, Label = "Terreno 4", Address = "Norte", Position = new Position(6.201094, -75.580370) }
            };

            foreach (var pin in pins)
            {
                // Podemos usar FromBundle, FromStream o FromView (custom view)
                //pin.Icon = BitmapDescriptorFactory.FromBundle("coffee_pin.png");
                map.Pins.Add(pin);
            }
        }
        
    }
}