﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace Proyecto_Integrador.Paginas
{
    class Elemento  //Eventos
    {
        public string Fecha { get; set; }
        public string Hora { get; set; }
        public string Ubicacion { get; set; }
        public int numParticipantes { get; set; }
        public string Tipo { get; set; }
        public string Arbol { get; set; }
    }
}
