﻿using Proyecto_Integrador.Models;
using Proyecto_Integrador.Repositories;
using Proyecto_Integrador.ViewModels;
using Proyecto_Integrador.Views;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Proyecto_Integrador.Paginas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Camara_Datos : ContentPage
    {
        List<RespuestaItem> respuesta1;
        List<RespuestaItem> respuesta2;
        List<RespuestaItem> respuesta3;
        public Camara_Datos()
        {
            InitializeComponent();
            InitApp();
        }
        //Metodo para el boton que activa la cámara
        private async void HacerFoto(object sender, EventArgs e)
        {
            var opciones_almacenamiento = new StoreCameraMediaOptions()
            {
                SaveToAlbum = true,
                Name = "MiFoto.jpg"
            };
            var foto = await CrossMedia.Current.TakePhotoAsync(opciones_almacenamiento);
            MiImagen.Source = ImageSource.FromStream(() =>
            {
                var stream = foto.GetStream();
                foto.Dispose();
                return stream;
            });
        }

        // Metodo para el boton que entra a la galeria de imagenes
        private async void ElegirImagen(object sender, EventArgs e)
        {
            if (CrossMedia.Current.IsTakePhotoSupported)
            {
                var imagen = await CrossMedia.Current.PickPhotoAsync();
                if (imagen != null)
                {
                    MiImagen.Source = ImageSource.FromStream(() =>
                    {
                        var stream = imagen.GetStream();
                        imagen.Dispose();
                        return stream;

                    });
                }
            }
        }
        private void Tomar_Foto_Clicked(object sender, EventArgs e)
        {

        }
        private void InitApp()
        {
            respuesta1 = new List<RespuestaItem>();
            respuesta1.Add(new RespuestaItem {id= 1, Name = "si"});
            respuesta1.Add(new RespuestaItem { id = 2, Name = "no" });
            respuesta1.Add(new RespuestaItem { id = 3, Name = "tal vez"});
            respuesta1.Add(new RespuestaItem { id = 4, Name = "depronto"});

            respuesta2 = new List<RespuestaItem>();
            respuesta2.Add(new RespuestaItem { id = 1, Name = "si" });
            respuesta2.Add(new RespuestaItem { id = 2, Name = "no" });
            
            respuesta3 = new List<RespuestaItem>();
            respuesta3.Add(new RespuestaItem { id = 1, Name = "si" });
            respuesta3.Add(new RespuestaItem { id = 2, Name = "no" });
           

            Pregunta1.ItemsSource=respuesta1;

            Pregunta2.ItemsSource = respuesta2;

            Pregunta3.ItemsSource = respuesta3;

        }


        /*
        private async void _button_Clicked1(object sender, EventArgs e)
        {
            try
            {
                _entry1.Text = Pregunta1.SelectedItem.ToString();
                await DisplayAlert("Mensaje", "¡Registrada la respuesta!", "Aceptar");
            }
            catch (Exception error) 
            {
            
            await DisplayAlert("Error", "¡Registre una respuesta!", "Aceptar");
            }
           
        }
        
        private async void _button_Clicked2(object sender, EventArgs e)
        {
            try
            {
                _entry2.Text = Pregunta2.SelectedItem.ToString();
                 await DisplayAlert("Mensaje", "¡Registrada la respuesta!", "Aceptar");
            }
            catch (Exception error)
            {

                await DisplayAlert("Error", "¡Registre una respuesta!", "Aceptar");
            }
        }*/
        private async void _button_Clicked3(object sender, EventArgs e)
        {
            try
            {
                _entry1.Text = Pregunta1.SelectedItem.ToString();
                _entry2.Text = Pregunta2.SelectedItem.ToString();
                _entry3.Text = Pregunta3.SelectedItem.ToString();
            await DisplayAlert("Mensaje", "¡ Se registro corectamente las respuestas!", "Aceptar");
            }
            catch (Exception error)
            {

                await DisplayAlert("Error", "¡Falta registrar una respuesta!", "Aceptar");
            }
        }


        private async void Confirmacion_Clicked(object sender, EventArgs e)
        {
            
            bool res = await DisplayAlert("Mensaje", "¡Registro confirmado Aquí cabe un árbol!", "Aceptar", "Cancelar");
        }
    }
    public class RespuestaItem
    {
        public string Name
        {
            get;
            set;
        }
        public int id
        {
            get;
            set;
        }
        public override string ToString()
        {
            return this .id + "." +this.Name;
        }
        
    }
}