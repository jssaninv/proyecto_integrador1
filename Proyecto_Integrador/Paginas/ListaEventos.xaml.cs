﻿using System;
using System.Collections.Generic;
using Proyecto_Integrador.ViewModels;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Proyecto_Integrador.Paginas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListaEventos : ContentPage
    {
        public ListaEventos()
        {
            InitializeComponent();
            ObservableCollection<Elemento> listaElemento = new ObservableCollection<Elemento>(new ServicioEvento().ConsultarElemento());
            ListEventos.ItemsSource = listaElemento;
        }

        private void btnAsistir_Clicked(object sender, EventArgs e)
        {

        }
    }
}