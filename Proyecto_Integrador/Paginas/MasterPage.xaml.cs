﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Proyecto_Integrador.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Proyecto_Integrador.Paginas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterPage : MasterDetailPage
    {
        
        public MasterPage()
        {
            InitializeComponent();
            Init();
          
        }

        private void Init()
        {
             List<Menu> menu = new List<Menu>(){
                 new Menu() { MenuTitle= "Inicio", MenuDetail= "Regresa a la pagina principal", icon="casa.png"},
                 new Menu() {MenuTitle= "¿Donde cabe un árbol?", MenuDetail= "Ingresar a la pagina de adicionar dirección", icon="Punto_mapa.png"},
                  new Menu() {MenuTitle= "Mis lugares registrados", MenuDetail= "Ver tus lugares registrados", icon="Terreno.png"},
                  new Menu() {MenuTitle= "Eventos", MenuDetail= "Interaccion con los eventos", icon="Evento.png"}
                  //new Menu() {MenuTitle= "Localizaciones", MenuDetail= "Interaccion con los eventos", icon="Evento.png"}
             };
            /*List<String> menu = new List<String>()
            {
                "pap",
                "pip"
            };*/
           
            ListMenu.ItemsSource = menu;
            Device.OnPlatform(iOS: () =>
            {
                ListMenu.Margin = new Thickness(0, 21, 0, 0);
            }, Android: () =>
            {
                ListMenu.Margin = new Thickness(0, 21, 0, 0);
            });

            // Detail = new NavigationPage(new Login());
            Detail = new NavigationPage(new Login());


        }
        

        void ListMenu_ItemSelected(object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
           var menu=  e.SelectedItem as Menu;
            if (menu != null)
            {
                if (menu.MenuTitle.Equals("Inicio"))
                {
                    IsPresented = false;
                    Detail = new NavigationPage(new Login());

                } else if (menu.MenuTitle.Equals("¿Donde cabe un árbol?"))
                {
                    IsPresented = false;
                    Detail = new NavigationPage(new Camara_Datos());
                }
                else if (menu.MenuTitle.Equals("Mis lugares registrados"))
                {
                    IsPresented = false;
                    Detail = new NavigationPage(new LugaresRegistrados());
                }
                else if (menu.MenuTitle.Equals("Eventos"))
                {
                    IsPresented = false;
                    Detail = new NavigationPage(new Eventos());

                }
                
            }
        }
    }
    public class Menu
    {
       
        public string MenuTitle
        {
            get;
            set;
        }
        public string MenuDetail
        {
            get;
            set;
        }
        public ImageSource icon
        {
            get;
            set;
        }
        

    }
}