﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Proyecto_Integrador.Paginas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LugaresRegistrados : ContentPage
    {
        public LugaresRegistrados()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ListaLugares lugares = new ListaLugares();
            ListLugares.ItemsSource = lugares._lugares;
            ListLugares.ItemSelected += ListLugares_ItemSelected;
        }
        private async void ListLugares_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
           if (e.SelectedItem != null)
            {
                var Lugar = e.SelectedItem as Lugar;
                await DisplayAlert("Detalles", Lugar.Detalles, "Aceptar");
            }
        }      
    }
}