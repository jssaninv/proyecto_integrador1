﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_Integrador.Paginas
{
    class ServicioEvento
    {
        public List<Elemento> ConsultarElemento()
        {
            var lista = new List<Elemento>();
            lista.Add(new Elemento() { Fecha = "30-05-2020", Hora = "15:00", Ubicacion = "Comuna x", numParticipantes = 15, Tipo = "Caminata", Arbol = "N/A" });
            lista.Add(new Elemento() { Fecha = "12-06-2020", Hora = "9:00", Ubicacion = "Ciudad del río", numParticipantes = 30, Tipo = "Sembratçon", Arbol = "Naranjo" });
            lista.Add(new Elemento() { Fecha = "13-06-2020", Hora = "10:00", Ubicacion = "Belen", numParticipantes = 15, Tipo = "Sembratón", Arbol = "Naranjo" });
            lista.Add(new Elemento() { Fecha = "07-07-2020", Hora = "15:30", Ubicacion = "Comuna x", numParticipantes = 20, Tipo = "Caminata", Arbol = "N/A" });
            return lista;
        }
    }
}
