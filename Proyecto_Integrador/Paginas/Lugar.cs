﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Proyecto_Integrador.Paginas
{
    public class Lugar
    {
      public string Direccion { get; set; }
      public string Sector { get; set; }
      public string Barrio { get; set; }

      public string Detalles { get; set; }

    }

    public class ListaLugares
    {
        public List<Lugar> _lugares{ get; set; }
        public ListaLugares()
        {
            _lugares = new List<Lugar>();
            LoadLugares();
        }

        public void LoadLugares()
        {
            _lugares.Add(new Lugar
            {
                Direccion = "Cra 43a #38-58",
                Barrio = "San Diego",
                Sector = "Comuna 9",
                Detalles = "Hay 3 especies arboreas"
            });

            _lugares.Add(new Lugar
            {
                Direccion = "Cl 18",
                Barrio = "Ciudad del río",
                Sector = "Comuna 14",
                Detalles = "Hay 2 especies arboreas"
            });

            _lugares.Add(new Lugar
            {
                Direccion = "Cl 3b Sur #29C-135",
                Barrio = "Los Naranjos",
                Sector = "Comuna 14",
                Detalles = "Hay 5 especies arboreas diferentes"
            });

            _lugares.Add(new Lugar
            {
                Direccion = "Cra 40a #48-38",
                Barrio = "Salvador",
                Sector = "Comuna 9",
                Detalles = "Hay más de 8 especies arboreas"
            });
        }
    }
}
