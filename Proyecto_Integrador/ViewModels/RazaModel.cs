﻿using Proyecto_Integrador.Base;
using Proyecto_Integrador.Models;
using Proyecto_Integrador.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Proyecto_Integrador.ViewModels
{
    public class RazaModel : ViewModelBase
    {
        RepositoryRazas repo;
        public RazaModel()
        {
            this.repo = new RepositoryRazas();
            this.Raza = new Raza();
            
        }

        public Command InsertarRaza
        {
            get
            {
                return new Command(() =>
                {
                    this.repo.InsertarRaza(this.Raza.Codigo,
                        Raza.Nombre, Raza.Caracteristicas, Raza.tipo
                        );
                });
            }
        }
        
        public Command InsertarRespuestas
        {
            get
            {
                return new Command(() =>
                {
                    this.repo.InsertarRespuestas(this.formulario.ID,
                        formulario.Pregunta_1, formulario.Pregunta_2, formulario.Pregunta_3
                        );
                });
            }
        }
        
        public Command ModificarRaza
        {
            get
            {
                return new Command(() =>
                {
                    this.repo.ModificarRaza(this.Raza.Codigo,
                        Raza.Nombre, Raza.Caracteristicas, Raza.tipo
                        );
                });
            }
        }

        public Command EliminarRaza
        {
            get
            {
                return new Command(() =>
                {
                    this.repo.EliminarRaza(this.Raza.Codigo
                        );
                });
            }
        }
        private formulario _formulario;

        private Raza _Raza;
        
        public Raza Raza
        {
            get { return this._Raza; }
            set
            {
                this._Raza = value;
                OnPropertyChanged("Raza");
            }
        }

        public formulario formulario
        {
            get { return this._formulario; }
            set
            {
                this._formulario = value;
                OnPropertyChanged("formulario");
            }
        }
    }
}