﻿using Proyecto_Integrador.Paginas;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Proyecto_Integrador
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
           // MainPage = new Eventos();
            MainPage = new MasterPage();
            //MainPage = new Mapita();
            //MainPage = new NavigationPage( new Login());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
