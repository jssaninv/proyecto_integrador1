﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Proyecto_Integrador.Models
{
    [Table("Eventos")]
    public class Raza
    {
        [PrimaryKey, AutoIncrement]
        public int Codigo { get; set; }
        public string Nombre { get; set; }
        public string Caracteristicas { get; set; }
        public string tipo { get; set; }

    }
    [Table("Formulario")]
    public class formulario
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Pregunta_1 { get; set; }
        public string Pregunta_2 { get; set; }
        public string Pregunta_3 { get; set; }
    }

}