﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Proyecto_Integrador.Base
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyname)
        {
            PropertyChanged?.Invoke(this,
                new PropertyChangedEventArgs(propertyname));
        }
    }
}