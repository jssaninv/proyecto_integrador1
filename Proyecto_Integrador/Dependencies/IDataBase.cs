﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Proyecto_Integrador.Dependencies
{
    public interface IDataBase
    {
        SQLite.SQLiteConnection GetConnection();
    }
}
