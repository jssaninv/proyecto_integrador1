﻿using Proyecto_Integrador.Dependencies;
using Proyecto_Integrador.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace Proyecto_Integrador.Repositories
{
    public class RepositoryRazas
    {
        private SQLiteConnection cn;

        public RepositoryRazas()
        {
            this.cn = DependencyService.Get<IDataBase>().GetConnection();

        }

        //------------------MÉTODOS:
        public void CrearBBDD()
        {
            //this.cn.DropTable<Raza>();
            //this.cn.DropTable<Formu>();

            this.cn.CreateTable<Raza>();
            this.cn.CreateTable<formulario>();
            //this.cn.CreateTable<Formu>();

        }

        public List<formulario> GetRespuestas()
        {
            var consulta = from datos in cn.Table<formulario>()
                           select datos;
            return consulta.ToList();
        }
        public List<Raza> GetRazas()
        {
            var consulta = from datos in cn.Table<Raza>()
                           select datos;
            return consulta.ToList();
        }
        public formulario BuscarRegistro(int num)
        {
            var consulta = from datos in cn.Table<formulario>()
                           where datos.ID == num
                           select datos;
            return consulta.FirstOrDefault();
        }
        public Raza BuscarRaza(int num)
        {
            var consulta = from datos in cn.Table<Raza>()
                           where datos.Codigo == num
                           select datos;
            return consulta.FirstOrDefault();
        }
        public void InsertarRaza(int num, string nom, string car, string tip)
        {
            Raza raz = new Raza();
            raz.Caracteristicas = car;
            raz.Codigo = num;
            raz.Nombre = nom;
            raz.tipo = tip;
            this.cn.Insert(raz);
        }
        public void InsertarRespuestas(int id, string P1, string P2, string P3)
        {
            formulario form = new formulario();
            form.ID = id;
            form.Pregunta_1 = P1;
            form.Pregunta_2 = P2;
            form.Pregunta_3 = P3;
            this.cn.Insert(form);
        }

        public void ModificarRaza(int num, string nom, string car, string tip)
        {
            Raza raz = this.BuscarRaza(num);
            raz.Nombre = nom;
            raz.Caracteristicas = car;
            raz.tipo = tip;
            this.cn.Update(raz);
        }
        public void EliminarRaza(int num)
        {
            Raza raz = this.BuscarRaza(num);
            this.cn.Delete<Raza>(num);
        }

        //Métodos de la Tabla Formulas a partir de aquí
        /*public List<Formu> GetFormulas()
        {
            var consulta = from datos in cn.Table<Formu>()
                           select datos;
            return consulta.ToList();
        }
        public Formu BuscarFormu(int num)
        {
            var consulta = from datos in cn.Table<Formu>()
                           where datos.Codigo == num
                           select datos;
            return consulta.FirstOrDefault();
        }
        public void InsertarFormu(int num, string pUno, string pDos, string pTres, string locUno, string locDos)
        {
            Formu fm = new Formu();
            fm.PreguntaUno = pUno;
            fm.PreguntaDos = pDos;
            fm.PreguntaTres = pTres;
            fm.LocalizacionUno = locUno;
            fm.LocalizacionDos = locDos;
            fm.Codigo = num;
            this.cn.Insert(fm);
        }
        public void EliminarFormu(int num)
        {
            Formu fm = this.BuscarFormu(num);
            this.cn.Delete<Formu>(num);
        }
        public void ModificarFormu(int num, string pUno, string pDos, string pTres, string locUno, string locDos)
        {
            Formu fm = this.BuscarFormu(num);
            fm.PreguntaUno = pUno;
            fm.PreguntaDos = pDos;
            fm.PreguntaTres = pTres;
            fm.LocalizacionUno = locUno;
            fm.LocalizacionDos = locDos;
            this.cn.Update(fm);
        }
        public Formu BuscarL1(int num)
        {
            var consulta = from datos in cn.Table<Formu>()
                           where datos.Codigo == num
                           select datos;
            return consulta.FirstOrDefault();
        }
        public Formu BuscarL2(int num)
        {
            var consulta = from datos in cn.Table<Formu>()
                           where datos.Codigo == num
                           select datos;
            return consulta.FirstOrDefault();
        }

        public void InsertarFormu1()
        {
            Formu fm = new Formu();
            fm.PreguntaUno = "aaa";
            fm.PreguntaDos = "bbb";
            fm.PreguntaTres = "ccc";
            fm.LocalizacionUno = "5555";
            fm.LocalizacionDos = "7777";
            fm.Codigo = 1;
            this.cn.Insert(fm);
        }*/
    }
}