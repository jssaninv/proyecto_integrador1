﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Proyecto_Integrador
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        int count = 0; //Contador para en numero de clicks
        void Handle_Clicked(object sender, System.EventArgs e)//metodo para la la acción del boton
        {
            count++;
            ((Button)sender).Text = $"Has marcado {count} ubicacion(es).";
        }
        public MainPage()
        {
            InitializeComponent();
        }

    }
}
